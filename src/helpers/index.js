// eslint-disable-next-line import/prefer-default-export
export const getId = (object, userName) => {
  const userLocation = object.filter(item => item.name === userName)[0]
    .location;

  return userLocation.substring(userLocation.length - 36);
};

export const getCoordinates = string => {
  if (!string) {
    return {
      lat: 0,
      lng: 0
    };
  }

  const regExp = /\(([^)]+)\)/;
  const matches = regExp.exec(string);
  const location = matches[1].split(" ");
  return {
    lat: Number(location[1]),
    lng: Number(location[0])
  };
};
