import React from "react";
import PropTypes from "prop-types";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { withStyles } from "material-ui/styles";

/* Components */
import Home from "./components/Home/";
import Survivors from "./components/Survivors/";
import AddSurvivor from "./components/AddSurvivor/";
import ReportInfected from "./components/ReportInfected";
import UpdateLocation from "./components/UpdateLocation";
import TradeItems from "./components/TradeItems";
import Reports from "./components/Reports";
import Header from "./components/Header";
import TopBar from "./components/Header/AppBar";

const styles = theme => ({
  appFrame: {
    height: "100%",
    overflow: "hidden",
    position: "relative",
    display: "flex",
    width: "100%"
  },
  content: {
    marginTop: theme.spacing.unit * 11,
    marginLeft: 260,
    flexGrow: 2,
    padding: theme.spacing.unit * 5,
    minHeight: "calc(100vh - 88px)"
  }
});

const Routes = props => (
  <Router>
    <div className={props.classes.appFrame}>
      <TopBar />
      <Header />
      <main className={props.classes.content}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/add" component={AddSurvivor} />
          <Route path="/survivors" component={Survivors} />
          <Route path="/report" component={ReportInfected} />
          <Route path="/update" component={UpdateLocation} />
          <Route path="/trade" component={TradeItems} />
          <Route path="/reports" component={Reports} />
          {/* catch any invalid URLs */}
          <Redirect to="/" />
        </Switch>
      </main>
    </div>
  </Router>
);

Routes.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(Routes);
