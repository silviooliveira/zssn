import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, getFormValues } from "redux-form";
import { connect } from "react-redux";
import { withStyles } from "material-ui/styles";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography";
import { Alert } from "mdi-material-ui";

import {
  getProperties,
  saveTotals,
  getLabels,
  saveActiveProperties,
  clearActiveProperties
} from "../../store/actions/trade";
import Button from "material-ui/Button";
import validate from "./validate";
import IntegrationDownshift from "../ReportInfected/IntegrationDownshift";

import ItemsTable from "./ItemsTable";
import InventoryFields from "./InventoryFields";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 36,
    paddingBottom: 16,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  }),
  flexRow: {
    display: "flex",
    width: "100%",
    flexDirection: "row"
  },
  flexInside: {
    flex: 1,
    flexBasis: "50%"
  },
  fieldset: {
    display: "flex",
    flexDirection: "row",
    width: `calc(100% + 16px)`,
    margin: "0 -8px 24px -8px"
  },
  downshiftBox: {
    display: "block",
    flex: "1 0 50%",
    padding: "0 15px"
  },
  tradeBox: {
    display: "block",
    flexBasis: "50%",
    padding: "0 15px"
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 36,
    fontSize: theme.typography.caption.fontSize,
    textTransform: "uppercase"
  },
  button: {
    margin: theme.spacing.unit
  },
  icon: {
    marginRight: 16
  },
  alert: {
    display: "flex",
    alignItems: "center"
  }
});

class TradeForm extends Component {
  componentDidMount() {
    this.props.dispatch(getLabels());
  }

  handleSelect = (value, type) => {
    const { buyer, seller } = this.props.activeProperties;

    if (buyer && seller) {
      this.clearFields();
    }

    const userId = this.props.labels[value].id;
    this.props.dispatch(getProperties(userId, value)).then(() => {
      if (!this.props.getPropertiesError) {
        this.props.dispatch(saveActiveProperties(value, type));
      }
    });
  };

  clearFields = () => {
    this.props.reset();
    this.props.dispatch(clearActiveProperties("buyer"));
    this.props.dispatch(clearActiveProperties("seller"));
  };

  getTotal = (values, type) => {
    const object = { ...values };
    const getTotal = (type, string) => Number(object[`${type}${string}`]) || 0;

    const water = getTotal(type, "Water");
    const food = getTotal(type, "Food");
    const ammunition = getTotal(type, "Ammunition");
    const medication = getTotal(type, "Medication");

    const totals = water * 4 + food * 3 + medication * 2 + ammunition * 1;

    this.props.dispatch(saveTotals(type, totals));
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.formValues !== this.props.formValues) {
      this.getTotal(nextProps.formValues, "buyer");
      this.getTotal(nextProps.formValues, "seller");
      if (!nextProps.formValues || !nextProps.formValues.buyer) {
        this.props.dispatch(clearActiveProperties("buyer"));
        this.props.dispatch(saveTotals("buyer", 0));
      }
      if (!nextProps.formValues || !nextProps.formValues.seller) {
        this.props.dispatch(clearActiveProperties("seller"));
        this.props.dispatch(saveTotals("seller", 0));
      }
    }
  }

  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      items,
      classes,
      totals
    } = this.props;
    const { buyer, seller } = this.props.activeProperties;

    const totalBuyer = totals.buyer;
    const totalSeller = totals.seller;

    return (
      <Paper className={classes.root} elevation={4}>
        <form onSubmit={handleSubmit}>
          <div className={classes.fieldset}>
            <div className={classes.downshiftBox}>
              <Typography
                className={classes.title}
                variant="title"
                gutterBottom
              >
                I am ...
              </Typography>
              <IntegrationDownshift
                name="buyer"
                inputName="buyer"
                label="Start typing to see a list of names"
                items={items}
                onValueSelect={this.handleSelect}
              />
              {this.props.getPropertiesError && (
                <Typography
                  className={classes.alert}
                  variant="body2"
                  gutterBottom
                >
                  <Alert className={classes.icon} />
                  We have no connection now. Try Later.
                </Typography>
              )}
              {buyer && <ItemsTable data={buyer} />}
            </div>
            <div className={classes.downshiftBox}>
              <Typography
                className={classes.title}
                variant="title"
                gutterBottom
              >
                And I want to trade with ...
              </Typography>
              <IntegrationDownshift
                name="seller"
                inputName="seller"
                label="Start typing to see a list of names"
                items={items}
                onValueSelect={this.handleSelect}
              />
              {this.props.getPropertiesError && (
                <Typography
                  className={classes.alert}
                  variant="body2"
                  gutterBottom
                >
                  <Alert className={classes.icon} />
                  We have no connection now. Try Later.
                </Typography>
              )}
              {seller && <ItemsTable data={seller} />}
            </div>
          </div>
          <div className={classes.fieldset}>
            <div className={classes.tradeBox}>
              <Typography
                className={classes.title}
                variant="title"
                gutterBottom
              >
                I have to offer ...
              </Typography>
              <InventoryFields
                data={buyer}
                type="buyer"
                total={totalBuyer}
                formValues={this.props.formValues}
              />
            </div>
            <div className={classes.tradeBox}>
              <Typography
                className={classes.title}
                variant="title"
                gutterBottom
              >
                In return I want ...
              </Typography>
              <InventoryFields
                data={seller}
                type="seller"
                total={totalSeller}
                formValues={this.props.formValues}
              />
            </div>
          </div>
          <div className={classes.fieldset}>
            <Button
              className={classes.button}
              type="submit"
              variant="raised"
              color="primary"
              disabled={!!(pristine || submitting)}
            >
              Request Trade
            </Button>
            <Button
              className={classes.button}
              onClick={this.clearFields}
              variant="raised"
              color="secondary"
              disabled={!!(pristine || submitting)}
            >
              Clear Fields
            </Button>
          </div>
        </form>
      </Paper>
    );
  }
}

const mapStateToProps = (state, props) => ({
  survivors: state.survivors.survivors,
  properties: state.trade.properties,
  totals: state.trade.totals,
  formValues: getFormValues("tradeForm")(state),
  labels: state.trade.labels,
  activeProperties: state.trade.activeProperties,
  getPropertiesError: state.trade.getPropertiesError
});

const { func, bool, object, array } = PropTypes;

TradeForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  reset: func.isRequired,
  submitting: bool.isRequired,
  classes: object,
  totals: object,
  items: array,
  dispatch: func,
  formValues: object,
  labels: object,
  activeProperties: object,
  getPropertiesError: bool
};

export default connect(mapStateToProps)(
  withStyles(styles)(
    reduxForm({
      form: "tradeForm", // a unique identifier for this form
      validate
    })(TradeForm)
  )
);
