import React from "react";
import PropTypes from "prop-types";
import { Field } from "redux-form";
import { withStyles } from "material-ui/styles";
import _ from "lodash";
import TextField from "material-ui/TextField";
import { InputAdornment } from "material-ui/Input";
import { FormHelperText, FormControl } from "material-ui/Form";
import { Water, Pistol, Pill, Food } from "mdi-material-ui";
import Typography from "material-ui/Typography";

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flex: 1
  },
  formsGroup: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap"
  },
  inventoryField: {
    flex: "0 0 45%",
    margin: "0 15px 20px 0"
  }
});

const renderTextField = ({
  input,
  label,
  max,
  placeholder,
  className,
  adornment,
  meta: { touched, error },
  ...custom
}) => (
  <FormControl className={className}>
    <TextField
      label={label}
      error={!!(touched && error)}
      placeholder="0"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">{adornment}</InputAdornment>
        )
      }}
      // eslint-disable-next-line
      inputProps={{
        min: 0,
        max
      }}
      {...input}
      {...custom}
    />
    {touched &&
      error && <FormHelperText id="name-error-text">{error}</FormHelperText>}
  </FormControl>
);

const { object, string, element, number } = PropTypes;

renderTextField.propTypes = {
  input: object.isRequired,
  label: string,
  max: number,
  placeholder: string,
  className: string,
  adornment: element,
  meta: object
};

const InventoryFields = props => {
  const { data, classes, total } = props;

  if (_.isEmpty(data)) {
    return (
      <Typography className={classes.title} variant="body2" gutterBottom>
        No survivor selected.
      </Typography>
    );
  }

  const adornments = {
    Water: <Water />,
    Food: <Food />,
    Ammunition: <Pistol />,
    Medication: <Pill />
  };

  const fields = _.map(data, (value, key) => (
    <Field
      key={key}
      name={props.type + key}
      component={renderTextField}
      label={key}
      max={value.quantity}
      adornment={adornments[key]}
      type="number"
      className={classes.inventoryField}
      InputLabelProps={{
        shrink: true
      }}
    />
  ));

  return (
    <div className={classes.root}>
      <h6>
        Click on the field to control the number of items you want to trade. You
        are limited to the number of items you have available. Below the fields
        you can see the total Points you have. To successfully accomplish the
        trade, both totals (the buyer and the seller) needs to match.{" "}
      </h6>
      <div className={classes.formsGroup}>{fields}</div>
      <h5>Total Points: {total} </h5>
    </div>
  );
};

InventoryFields.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object,
  total: PropTypes.number,
  type: PropTypes.string
};

export default withStyles(styles)(InventoryFields);
