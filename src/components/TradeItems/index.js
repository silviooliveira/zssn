import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";
import { reset } from "redux-form";
import { getSurvivors } from "../../store/actions/survivors";
import { tradeProperties } from "../../store/actions/trade";

import TradeForm from "./TradeForm";
import SnackBar from "../general/SnackBar";
import Loader from "../general/Loader";
import Error from "../general/Error";

class TradeItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snackbarOpen: false,
      snackbarMessage: undefined
    };
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    this.loadData();
  }

  loadData(forceLoad) {
    const { props } = this;

    if ((forceLoad || !props.survivorsLoaded) && !this.isLoading) {
      this.isLoading = true;

      props
        .dispatch(getSurvivors())
        // This always runs
        .then(() => {
          this.isLoading = false;
        });
    }
  }

  handleReportSubmit = values => {
    const { buyer, seller } = this.props.totals;
    if (!buyer && !seller) {
      alert(
        "You didn't filled in the inventory fiels either on your side (buyers) or on your seller side!"
      );
      return;
    }

    if (buyer !== seller) {
      alert("Your Total Points and your seller's Total Points should match!");
      return;
    }

    const wanted = [];
    const payInReturn = [];

    _.forEach(values, (value, key) => {
      const string = key;
      if (
        string.indexOf("buyer") !== -1 &&
        string !== "buyer" &&
        value !== "0"
      ) {
        payInReturn.push(`${string.substr(5)}:${value};`);
      }
      if (
        string.indexOf("seller") !== -1 &&
        string !== "seller" &&
        value !== "0"
      ) {
        wanted.push(`${string.substr(6)}:${value};`);
      }
    });

    const sellerId = this.props.survivors.filter(
      item => item.name === values.seller
    )[0].id;

    const data = new FormData();
    data.append("consumer[name]", values.buyer);
    data.append("consumer[pick]", wanted.join(""));
    data.append("consumer[payment]", payInReturn.join(""));

    this.props
      .dispatch(tradeProperties(sellerId, data))
      .catch(e => {
        alert("Transfer was not possible");
      })
      // This always runs
      .then(() => {
        this.props.dispatch(reset("tradeForm"));
        if (this.props.tradeError) {
          this.setState({
            snackbarOpen: true,
            snackbarMessage:
              "Your request was not completed. One of the parts are probably infected."
          });
        } else {
          this.setState({
            snackbarOpen: true,
            snackbarMessage: "Your trade request was successfully completed."
          });
        }
      });
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      snackbarOpen: false,
      snackbarMessage: undefined
    });
  };

  render() {
    if (this.props.survivorsError) {
      return <Error />;
    }

    if (!this.props.survivorsLoaded) {
      return <Loader />;
    }

    const suggestions = this.props.survivors.map(item => ({
      label: item.name,
      id: item.id
    }));

    return (
      <Fragment>
        <TradeForm onSubmit={this.handleReportSubmit} items={suggestions} />
        <SnackBar
          open={this.state.snackbarOpen}
          handleClose={this.handleSnackBarClose}
          message={this.state.snackbarMessage}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  survivors: state.survivors.survivors,
  survivorsLoaded: state.survivors.survivorsLoaded,
  survivorsError: state.survivors.survivorsError,
  totals: state.trade.totals,
  tradeError: state.trade.tradeError,
  getPropertiesError: state.trade.getPropertiesError
});

TradeItems.propTypes = {
  survivors: PropTypes.array,
  survivorsLoaded: PropTypes.bool,
  totals: PropTypes.object,
  tradeError: PropTypes.bool,
  dispatch: PropTypes.func,
  survivorsError: PropTypes.bool
};

export default connect(mapStateToProps)(TradeItems);
