export default function(values) {
  const errors = {};
  const requiredFields = ["buyer", "seller"];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = "Required field";
    }
  });
  if (values.seller === values.buyer) {
    errors.seller = "Values can not be equal";
  }
  return errors;
}
