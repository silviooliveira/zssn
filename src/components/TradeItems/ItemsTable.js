import React, { Fragment } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { withStyles } from "material-ui/styles";
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "material-ui/Table";
import Typography from "material-ui/Typography";
import Paper from "material-ui/Paper";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 300
  },
  cell: {
    padding: "14px 20px"
  },
  title: {}
});

const ItemsTable = props => {
  const { classes, data } = props;

  if (_.isEmpty(data)) {
    return (
      <Typography className={classes.title} variant="body2" gutterBottom>
        This survivor has no items in his/her inventoy.
      </Typography>
    );
  }
  const rows = _.map(data, (value, key) => (
    <TableRow key={key}>
      <TableCell className={classes.cell}>{key}</TableCell>
      <TableCell className={classes.cell} numeric>
        {value.quantity}
      </TableCell>
      <TableCell className={classes.cell} numeric>
        {value.pointPerItem}
      </TableCell>
      <TableCell className={classes.cell} numeric>
        {value.totalPoints}
      </TableCell>
    </TableRow>
  ));
  return (
    <Fragment>
      <Typography className={classes.title} variant="body2" gutterBottom>
        This survivor currently owns:
      </Typography>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.cell}>Item</TableCell>
              <TableCell className={classes.cell} numeric>
                Quantity
              </TableCell>
              <TableCell className={classes.cell} numeric>
                Points per Item
              </TableCell>
              <TableCell className={classes.cell} numeric>
                Total Points
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{rows}</TableBody>
        </Table>
      </Paper>
    </Fragment>
  );
};

ItemsTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object
};

export default withStyles(styles)(ItemsTable);
