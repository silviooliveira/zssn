import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { reduxForm } from "redux-form";
import Button from "material-ui/Button";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography";

import IntegrationDownshift from "./IntegrationDownshift";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 24,
    paddingBottom: 16,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  }),
  fieldset: {
    display: "flex",
    width: `calc(100% + 16px)`,
    margin: "0 -8px 24px -8px"
  },
  button: {
    margin: theme.spacing.unit
  },
  downshiftBox: {
    display: "block",
    flexBasis: "50%",
    padding: "0 15px"
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 36,
    fontSize: theme.typography.caption.fontSize,
    textTransform: "uppercase"
  }
});

const ReportForm = withStyles(styles)(props => {
  const { handleSubmit, pristine, reset, submitting, items } = props;
  return (
    <Paper className={props.classes.root} elevation={4}>
      <form onSubmit={handleSubmit}>
        <div
          className={props.classes.fieldset}
          style={{ flexDirection: "row" }}
        >
          <div className={props.classes.downshiftBox}>
            <Typography
              className={props.classes.title}
              variant="title"
              gutterBottom
            >
              I am ...
            </Typography>
            <IntegrationDownshift
              name="survivor"
              inputName="survivor"
              label="Enter you name"
              items={items}
            />
          </div>
          <div className={props.classes.downshiftBox}>
            <Typography
              className={props.classes.title}
              variant="title"
              gutterBottom
            >
              And I want to report ...
            </Typography>
            <IntegrationDownshift
              name="infected"
              inputName="infected"
              label="Enter the name of the infected survivor"
              items={items}
            />
          </div>
        </div>
        <div className={props.classes.fieldset}>
          <Button
            className={props.classes.button}
            type="submit"
            variant="raised"
            color="primary"
            disabled={!!(pristine || submitting)}
          >
            Report Infected
          </Button>
          <Button
            className={props.classes.button}
            onClick={reset}
            variant="raised"
            color="secondary"
            disabled={!!(pristine || submitting)}
          >
            Clear Values
          </Button>
        </div>
      </form>
    </Paper>
  );
});

const { func, bool, object, array } = PropTypes;

ReportForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  reset: func.isRequired,
  submitting: bool.isRequired,
  classes: object,
  items: array.isRequired
};

export default reduxForm({
  form: "reportForm" // a unique identifier for this form
})(ReportForm);
