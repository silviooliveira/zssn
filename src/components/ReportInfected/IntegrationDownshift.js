import React from "react";
import PropTypes from "prop-types";
import Downshift from "downshift";
import { Field } from "redux-form";
import { withStyles } from "material-ui/styles";
import TextField from "material-ui/TextField";
import { FormControl, FormHelperText } from "material-ui/Form";
import Paper from "material-ui/Paper";
import { MenuItem } from "material-ui/Menu";

function renderInput(inputProps) {
  const {
    InputProps,
    classes,
    name,
    touched,
    error,
    pristine,
    ...other
  } = inputProps;

  return (
    <FormControl fullWidth>
      <TextField
        error={!!(!pristine && error)}
        InputProps={{
          classes: {
            root: classes.inputRoot
          },
          name,
          ...InputProps
        }}
        {...other}
      />
      <FormHelperText>{!pristine && error && error}</FormHelperText>
    </FormControl>
  );
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem
}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion.label) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.label}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
    >
      {suggestion.label}
    </MenuItem>
  );
}

renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired
};

function getSuggestions(inputValue, suggestions) {
  let count = 0;

  return suggestions.filter(suggestion => {
    const keep =
      (!inputValue ||
        suggestion.label.toLowerCase().indexOf(inputValue.toLowerCase()) !==
          -1) &&
      count < 5;

    if (keep) {
      count += 1;
    }

    return keep;
  });
}

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  container: {
    flexGrow: 1,
    position: "relative"
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  inputRoot: {
    flexWrap: "wrap"
  }
});

function DownshiftList(props) {
  const { classes } = props;
  const {
    input,
    items,
    label,
    inputName,
    meta: { touched, error, pristine }
  } = props;

  const selectProps = {};

  if (props.onValueSelect)
    selectProps.onSelect = selectedItem =>
      props.onValueSelect(selectedItem, inputName);

  return (
    <div className={classes.root}>
      <Downshift
        {...input}
        {...selectProps}
        onStateChange={({ inputValue }) => input.onChange(inputValue)}
        selectedItem={input.value}
      >
        {({
          getInputProps,
          getItemProps,
          isOpen,
          inputValue,
          selectedItem,
          highlightedIndex
        }) => (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              name: inputName,
              classes,
              touched,
              error,
              pristine,
              InputProps: getInputProps({
                placeholder: label,
                name: inputName,
                id: "integration-downshift-simple"
              })
            })}
            {isOpen ? (
              <Paper className={classes.paper} square>
                {getSuggestions(inputValue, items).map((suggestion, index) =>
                  renderSuggestion({
                    suggestion,
                    index,
                    itemProps: getItemProps({ item: suggestion.label }),
                    highlightedIndex,
                    selectedItem
                  })
                )}
              </Paper>
            ) : null}
          </div>
        )}
      </Downshift>
    </div>
  );
}

const { object, func, string, array } = PropTypes;

DownshiftList.propTypes = {
  classes: object,
  input: object,
  items: array,
  label: string,
  inputName: string,
  meta: object,
  onValueSelect: func
};

const IntegrationDownshift = props => (
  <Field component={DownshiftList} {...props} />
);

export default withStyles(styles)(IntegrationDownshift);
