import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { reset } from "redux-form";
import { getSurvivors } from "../../store/actions/survivors";
import { flagSurvivor } from "../../store/actions/infected";

import { getId } from "../../helpers";

import ReportForm from "./ReportForm";
import SnackBar from "../general/SnackBar";
import Loader from "../general/Loader";
import Error from "../general/Error";

class ReportInfected extends Component {
  constructor(props) {
    super(props);
    this.isLoading = false;
    this.state = {
      snackbarOpen: false,
      snackbarMessage: undefined
    };
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    this.loadData();
  }

  loadData(forceLoad) {
    const { props } = this;

    if ((forceLoad || !props.survivorsLoaded) && !this.isLoading) {
      this.isLoading = true;

      props.onGetSurvivors().then(() => {
        this.isLoading = false;
      });
    }
  }

  handleReportSubmit = values => {
    const { survivor, infected } = values;
    if (!survivor || !infected) {
      this.setState({
        snackbarOpen: true,
        snackbarMessage: "Fields can not be blank."
      });
      return;
    }

    if (survivor === infected) {
      this.setState({
        snackbarOpen: true,
        snackbarMessage: "Survivor and Infected can not be the same."
      });
      return;
    }

    const survivorID = getId(this.props.survivors, survivor);
    const infectedID = getId(this.props.survivors, infected);

    const data = new FormData();
    data.append("infected", infectedID);

    this.props
      .onFlagSurvivor(survivorID, data)
      // This always runs
      .then(() => {
        if (this.props.flagSurvivorError) {
          this.setState({
            snackbarOpen: true,
            snackbarMessage:
              "Your request coudl not be made. You might have already flagged this survivor."
          });
        } else {
          this.setState({
            snackbarOpen: true,
            snackbarMessage: "Survivor successfully reported"
          });
          this.props.onReset("reportForm");
        }
      });
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      snackbarOpen: false,
      snackbarMessage: undefined
    });
  };

  render() {
    if (this.props.survivorsError) {
      return <Error />;
    }

    if (!this.props.survivorsLoaded) {
      return <Loader />;
    }

    const suggestions = this.props.survivors.map(item => {
      const string = item.location;
      const id = string.substring(string.length - 36);
      return {
        label: item.name,
        id
      };
    });

    return (
      <div>
        <ReportForm onSubmit={this.handleReportSubmit} items={suggestions} />
        <SnackBar
          open={this.state.snackbarOpen}
          handleClose={this.handleSnackBarClose}
          message={this.state.snackbarMessage}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  survivors: state.survivors.survivors,
  survivorsLoaded: state.survivors.survivorsLoaded,
  flagSurvivorError: state.infected.flagSurvivorError,
  survivorsError: state.survivors.survivorsError
});

ReportInfected.propTypes = {
  survivors: PropTypes.array,
  survivorsLoaded: PropTypes.bool,
  flagSurvivorError: PropTypes.bool,
  survivorsError: PropTypes.bool,
  onReset: PropTypes.func,
  onFlagSurvivor: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  onFlagSurvivor: (survivorID, data) =>
    dispatch(flagSurvivor(survivorID, data)),
  onReset: form => dispatch(reset(form)),
  onGetSurvivors: () => dispatch(getSurvivors())
});

export default connect(mapStateToProps, mapDispatchToProps)(ReportInfected);
