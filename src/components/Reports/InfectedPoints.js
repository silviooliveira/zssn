import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Grid from "material-ui/Grid";
import Divider from "material-ui/Divider";
import Card, { CardContent } from "material-ui/Card";
import Typography from "material-ui/Typography";
import { Counter, Update } from "mdi-material-ui";
import green from "material-ui/colors/green";

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey[50]
  },
  paper: {
    paddingBottom: "12px !important"
  },
  box: {
    float: "left",
    width: 150,
    height: 150,
    background: `linear-gradient(60deg, ${green[400]}, ${green[600]})`,
    margin: "-30px 0px 0",
    borderRadius: 3,
    boxShadow: theme.shadows[15],
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    width: 100,
    textAlign: "right",
    float: "right",
    color: theme.palette.grey[500]
  },
  number: {
    fontSize: 100,
    lineHeight: 1,
    clear: "left",
    fontWeight: "bold",
    textAlign: "right",
    color: theme.palette.grey[800],
    letterSpacing: -6,
    "& span": {
      fontSize: 40,
      letterSpacing: 1
    }
  },
  icon: {
    width: 100,
    height: 100,
    fill: "#fff"
  },
  update: {
    color: theme.palette.grey[500],
    display: "flex",
    alignItems: "center"
  },
  updateIcon: {
    marginRight: 10
  },
  divider: {
    backgroundColor: theme.palette.grey[500],
    margin: "16px 0 6px"
  }
});

const InfectedPoints = props => {
  const { data } = props;
  return (
    <Grid item xs={12} sm={6}>
      <Card className={props.classes.root}>
        <CardContent className={props.classes.paper}>
          <div className={props.classes.box}>
            <Counter className={props.classes.icon} />
          </div>
          <Typography className={props.classes.title} color="textSecondary">
            {data.description}
          </Typography>
          <Typography
            className={props.classes.number}
            variant="display1"
            component="h1"
          >
            {data.total_points_lost}
          </Typography>
          <Divider className={props.classes.divider} />
          <Typography className={props.classes.update} color="textSecondary">
            {" "}
            <Update className={props.classes.updateIcon} /> Just Updated
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

InfectedPoints.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired
};

export default withStyles(styles)(InfectedPoints);
