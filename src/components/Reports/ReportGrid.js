import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Grid from "material-ui/Grid";
import Paper from "material-ui/Paper";

import InfectedReport from "./InfectedReport";
import NonInfectedReport from "./NonInfectedReport";
import PeopleInventory from "./PeopleInventory";
import InfectedPoints from "./InfectedPoints";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 24,
    paddingBottom: 24,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  })
});

const ReportGrid = props => (
  <Paper className={props.classes.root} elevation={4}>
    <Grid container spacing={40}>
      <InfectedReport data={props.data.infected} />
      <NonInfectedReport data={props.data.non_infected} />
      <PeopleInventory data={props.data.people_inventory} />
      <InfectedPoints data={props.data.infected_points} />
    </Grid>
  </Paper>
);

ReportGrid.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired
};

export default withStyles(styles)(ReportGrid);
