import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { connect } from "react-redux";
import { getReports } from "../../store/actions/reports";

import ReportGrid from "./ReportGrid";
import Loader from "../general/Loader";
import Error from "../general/Error";

class Reports extends Component {
  componentDidMount() {
    this.props.getReports();
  }

  render() {
    if (this.props.reportsError) {
      return <Error />;
    }

    if (_.isEmpty(this.props.reports)) {
      return <Loader />;
    }

    return <ReportGrid data={this.props.reports} />;
  }
}

const mapStateToProps = (state, props) => ({
  reports: state.reports.reports,
  reportsError: state.reports.reportsError
});

Reports.propTypes = {
  reports: PropTypes.object,
  getReports: PropTypes.func,
  reportsError: PropTypes.bool
};

export default connect(mapStateToProps, { getReports })(Reports);
