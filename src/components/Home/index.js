import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import { NavLink } from "react-router-dom";
import Logo from "../../assets/zssn.svg";

const styles = theme => ({
  root: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    maxWidth: 600,
    margin: "0 auto"
  },
  button: {
    margin: 8
  },
  heading: {
    fontWeight: theme.typography.fontWeightLight,
    marginBottom: 35,
    lineHeight: 1.6
  },
  display: {
    fontWeight: theme.typography.fontWeightMedium
  },
  logo: {
    width: 250,
    marginBottom: 50
  }
});

const Home = props => (
  <div className={props.classes.root}>
    <img className={props.classes.logo} src={Logo} alt="zssn logo" />
    <Typography
      variant="display2"
      gutterBottom
      align="center"
      className={props.classes.display}
    >
      Welcome to ZSSN
    </Typography>
    <Typography
      variant="title"
      gutterBottom
      align="center"
      className={props.classes.heading}
    >
      The world as we know it has fallen into an apocalyptic scenario. A
      laboratory-made virus is transforming human beings and animals into
      zombies, hungry for fresh flesh. Be a zombie resistance member to share
      resources between non-infected humans.
    </Typography>
    <div>
      <Button
        variant="raised"
        color="primary"
        component={NavLink}
        to="/add"
        className={props.classes.button}
      >
        Register Now
      </Button>
      <Button
        variant="raised"
        color="secondary"
        component={NavLink}
        to="/trade"
        className={props.classes.button}
      >
        Share Resources
      </Button>
    </div>
  </div>
);

Home.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Home);
