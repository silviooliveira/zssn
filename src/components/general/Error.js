import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import { Alert } from "mdi-material-ui";

const style = theme => ({
  text: {
    height: "100%",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    marginRight: 16
  }
});

const Error = props => (
  <Typography
    variant="headline"
    gutterBottom
    className={props.classes.text}
    color="secondary"
  >
    <Alert className={props.classes.icon} />
    It seems we have a network problem now. Please refresh or try later.
  </Typography>
);

Error.propTypes = {
  classes: PropTypes.object
};

export default withStyles(style)(Error);
