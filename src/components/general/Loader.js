import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { CircularProgress } from "material-ui/Progress";

const style = theme => ({
  progress: {
    height: "100%",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
});

const Loader = props => (
  <div className={props.classes.progress}>
    <CircularProgress color="primary" size={50} />
  </div>
);

Loader.propTypes = {
  classes: PropTypes.object
};

export default withStyles(style)(Loader);
