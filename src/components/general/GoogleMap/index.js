import React, { Component } from "react";
import PropTypes from "prop-types";
import scriptLoader from "react-async-script-loader";

import { mapStyles } from "./map_styles";

class MapContainer extends Component {
  /* Method to load GoogleMaps map */
  async loadMap() {
    const google = await window.google;

    const styledMapType = new google.maps.StyledMapType(mapStyles);

    const map = new google.maps.Map(this.refs.map, {
      zoom: 2,
      center: this.props.location,
      disableDefaultUI: true,
      mapTypeControlOptions: {
        mapTypeIds: ["roadmap", "styled_map"]
      }
    });

    map.mapTypes.set("styled_map", styledMapType);
    map.setMapTypeId("styled_map");

    const newMarker = new google.maps.Marker({
      position: this.props.location,
      map,
      draggable: true,
      icon: require("../../../assets/marker.png")
    });

    newMarker.addListener("dragend", () => {
      const latLng = newMarker.position;
      const currentLatitude = latLng.lat();
      const currentLongitude = latLng.lng();
      this.props.onCoordinatesChange({
        lat: currentLatitude,
        lng: currentLongitude
      });
    });
  }

  /* Upon mounting, check if GoogleMaps script has successfully loaded before
     trying to excute the method that builds the map.
  */
  componentDidMount() {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.loadMap();
    }
  }

  componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed, location }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) {
      // load finished
      if (isScriptLoadSucceed) {
        this.loadMap();
      } else this.props.onError();
    }
  }

  render() {
    return <div ref="map" style={this.props.styles} />;
  }
}

const { object, bool, func } = PropTypes;

MapContainer.propTypes = {
  location: PropTypes.object,
  isScriptLoaded: bool,
  isScriptLoadSucceed: bool,
  onError: func,
  styles: object,
  onCoordinatesChange: func
};

export default scriptLoader([
  "https://maps.googleapis.com/maps/api/js?key=AIzaSyCqTpVjBaiaJf7OGbEdLWlKcQUq3HwTlTw"
])(MapContainer);
