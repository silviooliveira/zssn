import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Button from "material-ui/Button";
import Snackbar from "material-ui/Snackbar";
import IconButton from "material-ui/IconButton";
import { Close } from "mdi-material-ui";

const styles = theme => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4
  }
});

const SnackBar = props => (
  <div>
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center"
      }}
      open={props.open}
      autoHideDuration={6000}
      onClose={props.handleClose}
      SnackbarContentProps={{
        "aria-describedby": "message-id"
      }}
      message={<span id="message-id">{props.message}</span>}
      action={[
        <Button
          key="undo"
          color="secondary"
          size="small"
          onClick={props.handleClose}
        >
          CLOSE
        </Button>,
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={props.classes.close}
          onClick={props.handleClose}
        >
          <Close />
        </IconButton>
      ]}
    />
  </div>
);

const { object, bool, func, string } = PropTypes;

SnackBar.propTypes = {
  classes: object.isRequired,
  open: bool.isRequired,
  handleClose: func.isRequired,
  message: string
};

export default withStyles(styles)(SnackBar);
