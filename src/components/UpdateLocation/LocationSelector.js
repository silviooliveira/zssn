import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";

import MapContainer from "../general/GoogleMap";

const styles = theme => ({
  fieldset: {
    display: "flex",
    width: `calc(100% + 16px)`,
    margin: "0 -8px 24px -8px",
    paddingLeft: 16,
    paddingRight: 16,
    flexDirection: "column"
  },
  locationContainer: {
    display: "flex",
    justifyContent: "space-between",
    margin: "8px 0"
  },
  locationBox: {
    flexBasis: "49%",
    backgroundColor: theme.palette.grey[600],
    padding: "8px 16px",
    borderRadius: "2px",
    boxShadow: theme.shadows[1]
  },
  locationTitle: {
    textTransform: "uppercase",
    margin: "8px 0"
  },
  locationData: {
    margin: 0,
    fontStyle: "italic",
    fontSize: "0.9rem"
  }
});

const LocationSelector = props => (
  <Fragment>
    <div className={props.classes.fieldset}>
      <MapContainer
        location={props.location}
        styles={props.styles}
        onCoordinatesChange={props.onCoordinatesChange}
      />
      <Typography variant="caption" gutterBottom>
        * Drag the marker to choose a new location.
      </Typography>
      <div className={props.classes.locationContainer}>
        <div className={props.classes.locationBox}>
          <h6 className={props.classes.locationTitle}>Latitude</h6>
          <p className={props.classes.locationData}>
            {props.lat
              ? props.lat
              : "Please drag the marker to choose a location"}
          </p>
        </div>
        <div className={props.classes.locationBox}>
          <h6 className={props.classes.locationTitle}>Longitude</h6>
          <p className={props.classes.locationData}>
            {props.lng
              ? props.lng
              : "Please drag the marker to choose a location"}
          </p>
        </div>
      </div>
    </div>
    <div className={props.classes.fieldset}>
      <Button variant="raised" color="primary" onClick={props.updateLocation}>
        Update Location
      </Button>
    </div>
  </Fragment>
);

LocationSelector.propTypes = {
  classes: PropTypes.object,
  lng: PropTypes.number,
  lat: PropTypes.number,
  updateLocation: PropTypes.func,
  onCoordinatesChange: PropTypes.func,
  location: PropTypes.object,
  styles: PropTypes.object
};

export default withStyles(styles)(LocationSelector);
