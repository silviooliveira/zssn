import React from "react";
import PropTypes from "prop-types";
import { reduxForm } from "redux-form";
import { withStyles } from "material-ui/styles";
import Button from "material-ui/Button";
import IntegrationDownshift from "../ReportInfected/IntegrationDownshift";

const styles = theme => ({
  fieldGroup: {
    display: "flex",
    width: `calc(100% + 16px)`,
    margin: "0 -8px 24px -8px",
    padding: "40px 0 12px"
  },
  downshift: {
    flexGrow: 2,
    padding: "0 15px"
  },
  buttonGroup: {
    flexGrow: 0,
    padding: "0 15px"
  },
  button: {
    margin: "0 10px"
  }
});

const SearchForm = withStyles(styles)(props => {
  const { handleSubmit, pristine, submitting, items } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className={props.classes.fieldGroup}>
        <div className={props.classes.downshift}>
          <IntegrationDownshift
            name="survivor"
            inputName="survivor"
            label="Enter Survivor that needs to be updated"
            items={items}
          />
        </div>
        <div className={props.classes.buttonGroup}>
          <Button
            className={props.classes.button}
            type="submit"
            variant="raised"
            color="primary"
            disabled={!!(pristine || submitting)}
          >
            Search Name
          </Button>
        </div>
      </div>
    </form>
  );
});

const { func, bool, object } = PropTypes;

SearchForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  reset: func.isRequired,
  submitting: bool.isRequired,
  classes: object
};

export default reduxForm({
  form: "searchForm"
})(SearchForm);
