import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { connect } from "react-redux";
import { reset } from "redux-form";
import {
  getSurvivors,
  updateSurvivor,
  setActiveLocation,
  cleanActiveLocation,
  changeActiveLocation
} from "../../store/actions/survivors";
import Paper from "material-ui/Paper";

import SearchForm from "./SearchForm";
import LocationSelector from "./LocationSelector";
import SnackBar from "../general/SnackBar";
import Loader from "../general/Loader";
import Error from "../general/Error";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  })
});

class UpdateLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snackbarOpen: false,
      snackbarMessage: undefined
    };
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    this.loadData();
  }

  loadData(forceLoad) {
    const { props } = this;

    if ((forceLoad || !props.survivorsLoaded) && !this.isLoading) {
      this.isLoading = true;

      props
        .dispatch(getSurvivors())
        // This always runs
        .then(() => {
          this.isLoading = false;
        });
    }
  }

  handleClickSearch = values => {
    this.props.dispatch(cleanActiveLocation());

    const { survivor } = values;
    if (!survivor) {
      this.setState({
        snackbarOpen: true,
        snackbarMessage: "User can not be blank."
      });
      return;
    }

    this.props.dispatch(setActiveLocation(survivor));
  };

  onCoordinatesChange = coord => {
    this.props.dispatch(changeActiveLocation(coord));
  };

  handleUpdateLocation = () => {
    const { lat, lng } = this.props.activeSurvivor.coordinates;

    const survivorID = this.props.activeSurvivor.id;
    const lonlat = `Point(${lng} ${lat})`;

    const data = new FormData();
    data.append("person[lonlat]", lonlat);

    this.props
      .dispatch(updateSurvivor(survivorID, data))
      .catch(this.handleApiError)
      // This always runs
      .then(() => {
        this.loadData(true);
        this.props.dispatch(cleanActiveLocation());
        this.props.dispatch(reset("searchForm"));
        if (!this.props.updateSurvivorError) {
          this.setState({
            snackbarOpen: true,
            snackbarMessage: "Location updated successfully"
          });
        } else {
          this.setState({
            snackbarOpen: true,
            snackbarMessage:
              "We could not update the location. Please try later."
          });
        }
      });
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      snackbarOpen: false,
      snackbarMessage: undefined
    });
  };

  render() {
    if (this.props.survivorsError) {
      return <Error />;
    }

    if (!this.props.survivorsLoaded) {
      return <Loader />;
    }

    const mapStyle = {
      width: "100%",
      height: "300px"
    };

    const { activeSurvivor } = this.props;

    const suggestions = this.props.survivors.map(item => {
      const string = item.location;
      const id = string.substring(string.length - 36);
      return {
        label: item.name,
        id
      };
    });

    return (
      <Paper className={this.props.classes.root} elevation={4}>
        <SearchForm onSubmit={this.handleClickSearch} items={suggestions} />
        {activeSurvivor && (
          <LocationSelector
            location={activeSurvivor.coordinates}
            styles={mapStyle}
            onCoordinatesChange={this.onCoordinatesChange}
            name={activeSurvivor.name}
            lat={activeSurvivor.coordinates.lat}
            lng={activeSurvivor.coordinates.lng}
            updateLocation={this.handleUpdateLocation}
          />
        )}
        <SnackBar
          open={this.state.snackbarOpen}
          handleClose={this.handleSnackBarClose}
          message={this.state.snackbarMessage}
        />
      </Paper>
    );
  }
}

const mapStateToProps = (state, props) => ({
  survivors: state.survivors.survivors,
  survivorsLoaded: state.survivors.survivorsLoaded,
  activeSurvivor: state.survivors.activeSurvivor,
  survivorsError: state.survivors.survivorsError,
  updateSurvivorError: state.survivors.updateSurvivorError
});

UpdateLocation.propTypes = {
  survivors: PropTypes.array,
  survivorsLoaded: PropTypes.bool,
  classes: PropTypes.object,
  dispatch: PropTypes.func,
  activeSurvivor: PropTypes.object,
  survivorsError: PropTypes.bool,
  updateSurvivorError: PropTypes.bool
};

export default connect(mapStateToProps)(withStyles(styles)(UpdateLocation));
