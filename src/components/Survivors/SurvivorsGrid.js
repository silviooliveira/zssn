import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Grid from "material-ui/Grid";
import TextField from "material-ui/TextField";
import Paper from "material-ui/Paper";

import SurvivorsItem from "./SurvivorsItem";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 36,
    paddingBottom: 16,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  })
});

class SurvivorsGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: ""
    };
  }

  renderSurvivors = filter => {
    if (!filter) {
      return this.props.survivors.map(item => (
        <SurvivorsItem key={item.location} survivor={item} />
      ));
    }

    return this.props.survivors
      .filter(
        item => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
      )
      .map(item => <SurvivorsItem key={item.location} survivor={item} />);
  };

  render() {
    return (
      <Paper className={this.props.classes.root} elevation={4}>
        <TextField
          id="name-input"
          label="Filter By Name"
          type="text"
          margin="normal"
          value={this.state.filter}
          onChange={e => this.setState({ filter: e.target.value })}
          fullWidth
          style={{
            marginBottom: 60
          }}
        />

        <Grid container spacing={24}>
          {this.renderSurvivors(this.state.filter)}
        </Grid>
      </Paper>
    );
  }
}

SurvivorsGrid.propTypes = {
  classes: PropTypes.object.isRequired,
  survivors: PropTypes.array.isRequired
};

export default withStyles(styles)(SurvivorsGrid);
