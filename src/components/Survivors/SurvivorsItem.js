import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Grid from "material-ui/Grid";
import Card, { CardContent } from "material-ui/Card";
import Typography from "material-ui/Typography";
import Chip from "material-ui/Chip";
import {
  EmoticonSad,
  EmoticonHappy,
  HumanMale,
  HumanFemale
} from "mdi-material-ui";
import Avatar from "material-ui/Avatar";
import Tooltip from "material-ui/Tooltip";
import orange from "material-ui/colors/orange";
import blue from "material-ui/colors/blue";
import moment from "moment";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  caption: {
    marginTop: 20
  },
  infected: {
    backgroundColor: orange[800]
  },
  notInfected: {
    backgroundColor: blue[800]
  },
  chipInfected: {
    marginTop: 20,
    backgroundColor: orange[400]
  },
  chipNotInfected: {
    marginTop: 20,
    backgroundColor: blue[400]
  },
  flexbox: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-end"
  },
  icon: {
    fill: "#fff",
    width: 38,
    height: 38,
    transform: "translate(16px, 3px)"
  }
});

const SurvivorsItem = props => {
  const { survivor } = props;

  if (!survivor) {
    return null;
  }

  let name = "";

  if (survivor.name.length > 13) {
    name = (
      <Tooltip
        id="tooltip-top-start"
        title={survivor.name}
        placement="top-start"
      >
        <Typography variant="title">
          {`${survivor.name.substr(0, 13)}...`}
        </Typography>
      </Tooltip>
    );
  } else {
    name = <Typography variant="title">{survivor.name}</Typography>;
  }

  return (
    <Grid item xs={6} sm={4}>
      <Card className={props.classes.root}>
        <CardContent>
          {name}
          <Typography variant="headline" component="h2">
            {survivor.age}
          </Typography>
          <Typography variant="caption" className={props.classes.caption}>
            CREATED ON
          </Typography>
          <Typography component="p">
            {moment(survivor.updated_at).format("LL")}
          </Typography>
          <div className={props.classes.flexbox}>
            <Chip
              avatar={
                <Avatar
                  className={
                    survivor["infected?"]
                      ? props.classes.infected
                      : props.classes.notInfected
                  }
                >
                  {survivor["infected?"] ? <EmoticonSad /> : <EmoticonHappy />}
                </Avatar>
              }
              label={survivor["infected?"] ? "Infected" : "Not Infected"}
              className={
                survivor["infected?"]
                  ? props.classes.chipInfected
                  : props.classes.chipNotInfected
              }
            />
            {survivor.gender === "M" ? (
              <HumanMale className={props.classes.icon} />
            ) : (
              <HumanFemale className={props.classes.icon} />
            )}
          </div>
        </CardContent>
      </Card>
    </Grid>
  );
};

SurvivorsItem.propTypes = {
  classes: PropTypes.object.isRequired,
  survivor: PropTypes.object.isRequired
};

export default withStyles(styles)(SurvivorsItem);
