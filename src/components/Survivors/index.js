import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getSurvivors } from "../../store/actions/survivors";

import SurvivorsGrid from "./SurvivorsGrid";
import Loader from "../general/Loader";
import Error from "../general/Error";

class Survivors extends Component {
  constructor(props) {
    super(props);
    this.isLoading = false;
  }

  componentDidMount() {
    this.loadData(true);
  }

  componentDidUpdate() {
    this.loadData();
  }

  loadData(forceLoad) {
    const { props } = this;

    if ((forceLoad || !props.survivorsLoaded) && !this.isLoading) {
      this.isLoading = true;

      props.dispatch(getSurvivors()).then(() => {
        this.isLoading = false;
      });
    }
  }

  render() {
    if (this.props.survivorsError) {
      return <Error />;
    }
    if (!this.props.survivorsLoaded) {
      return <Loader />;
    }

    return <SurvivorsGrid survivors={this.props.survivors} />;
  }
}

const mapStateToProps = (state, props) => ({
  survivors: state.survivors.survivors,
  survivorsLoaded: state.survivors.survivorsLoaded,
  survivorsError: state.survivors.survivorsError
});

Survivors.propTypes = {
  survivors: PropTypes.array,
  survivorsLoaded: PropTypes.bool,
  survivorsError: PropTypes.bool
};

export default connect(mapStateToProps)(Survivors);
