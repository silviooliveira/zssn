import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import React from "react";
import { AddSurvivors } from "./index";
// import { shallow } from "enzyme";
import { createShallow } from "material-ui/test-utils";

configure({ adapter: new Adapter() });

console.error = jest.fn();

// FORM DATA MINIMAL MOCK
window.FormData = class FormDataMock {
  object = {};
  append(key, value) {
    this.object[key] = value;
  }
};

describe("AddSurvivors", () => {
  describe("submition", () => {
    let shallow;
    beforeEach(() => {
      shallow = createShallow();
      console.error.mockClear();
    });

    it("should call onAddSurvivor on form submit with right values", async () => {
      const submitted = {
        name: "Silvio",
        age: 35,
        sex: "M",
        water: 2,
        food: 3,
        ammunition: 0,
        medication: 0
      };

      const onReset = jest.fn();
      const onAddSurvivor = jest.fn(() => Promise.resolve());
      const onGetSurvivors = jest.fn(() => Promise.resolve());
      const onLocationChange = jest.fn(() => Promise.resolve());
      const onLocationClean = jest.fn(() => Promise.resolve());
      const wrapper = shallow(
        <AddSurvivors
          onAddSurvivor={onAddSurvivor}
          onReset={onReset}
          onGetSurvivors={onGetSurvivors}
          onLocationChange={onLocationChange}
          onLocationClean={onLocationClean}
        />
      );
      await wrapper
        .find("WithStyles(ReduxForm)")
        .props()
        .onSubmit(submitted);
      expect(onAddSurvivor).toHaveBeenCalledWith({
        object: {
          "person[name]": "Silvio",
          "person[age]": 35,
          "person[gender]": "M",
          items: "Water:2;Food:3;"
        }
      });
      //
      expect(onReset).toHaveBeenCalledWith("addSurvivorForm");
      expect(onGetSurvivors).toHaveBeenCalled();
    });

    it("should handle api errors", async () => {
      const submitted = {};
      const onReset = jest.fn();
      const onAddSurvivor = jest.fn(() => Promise.resolve());
      const onGetSurvivors = jest.fn(() => Promise.resolve());
      const onLocationChange = jest.fn(() => Promise.resolve());
      const onLocationClean = jest.fn(() => Promise.resolve());
      const wrapper = shallow(
        <AddSurvivors
          onAddSurvivor={onAddSurvivor}
          onReset={onReset}
          onGetSurvivors={onGetSurvivors}
          onLocationChange={onLocationChange}
          onLocationClean={onLocationClean}
        />
      );
      await wrapper
        .find("WithStyles(ReduxForm)")
        .props()
        .onSubmit(submitted);
      expect(console.error).toHaveBeenCalledTimes(1);
      expect(onReset).toHaveBeenCalledWith("addSurvivorForm");
      expect(onGetSurvivors).toHaveBeenCalled();
    });
  });
});
