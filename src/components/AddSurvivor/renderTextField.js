import React from "react";
import PropTypes from "prop-types";
import TextField from "material-ui/TextField";
import { FormControl, FormHelperText } from "material-ui/Form";

const renderTextField = ({
  input,
  label,
  placeholder,
  meta: { touched, error },
  className,
  ...custom
}) => (
  <FormControl className={className}>
    <TextField
      label={label}
      placeholder={placeholder}
      error={!!(touched && error)}
      inputProps={{
        min: 0
      }}
      {...input}
      {...custom}
    />
    {touched &&
      error && <FormHelperText id="name-error-text">{error}</FormHelperText>}
  </FormControl>
);

const { object, string } = PropTypes;

renderTextField.propTypes = {
  input: object,
  meta: object,
  label: string,
  className: string,
  placeholder: string
};

export default renderTextField;
