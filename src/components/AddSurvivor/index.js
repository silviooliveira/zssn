import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { connect } from "react-redux";
import { reset } from "redux-form";

import {
  addSurvivor,
  getSurvivors,
  setLocation,
  cleanLocation
} from "../../store/actions/survivors";
import SurvivorForm from "./SurvivorForm";
import SnackBar from "../general/SnackBar";

const PROPERTIES = ["water", "food", "ammunition", "medication"];

export class AddSurvivors extends Component {
  constructor(props) {
    super(props);
    this.isLoading = false;
    this.state = {
      snackbarOpen: false,
      snackbarMessage: undefined
    };
  }

  getLocationFromDevice = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          let coordinates = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          this.props.onLocationChange(coordinates, "default");
        },
        () => {
          let coordinates = {
            lat: 0,
            lng: 0
          };
          this.props.onLocationChange(coordinates, "default");
        }
      );
    } else {
      let coordinates = {
        lat: 0,
        lng: 0
      };
      this.props.onLocationChange(coordinates, "default");
    }
  };

  componentDidMount() {
    this.getLocationFromDevice();
  }

  /* Auxiliar method to generate ITEMS string for POST Request */
  getItems = (values, string) => {
    if (values[string]) {
      return `${_.capitalize(string)}:${values[string]};`;
    }
    return false;
  };

  /* Method to submit form */
  submitSurvivor = values => {
    if (_.isEmpty(values)) {
      console.error("Values must not be empty");
    }
    const itemsArray = [];
    PROPERTIES.forEach(item => {
      const itemText = this.getItems(values, item);
      if (itemText) {
        itemsArray.push(itemText);
      }
    });

    const data = new FormData();
    data.append("person[name]", values.name);
    data.append("person[age]", values.age);
    data.append("person[gender]", values.sex);
    data.append("items", itemsArray.join(""));

    if (this.props.choosenLocation) {
      if (this.props.choosenLocation.lat && this.props.choosenLocation.lng) {
        let lonlat = `Point(${this.props.choosenLocation.lng} ${
          this.props.choosenLocation.lat
        })`;
        data.append("person[lonlat]", lonlat);
      } else {
        let lonlat = `Point(${this.props.defaultLocation.lng} ${
          this.props.defaultLocation.lat
        })`;
        data.append("person[lonlat]", lonlat);
      }
    }

    return (
      this.props
        .onAddSurvivor(data)
        // This always runs
        .then(() => {
          this.getLocationFromDevice();
          this.props.onReset("addSurvivorForm");
          this.props.onLocationClean();
          if (!this.props.addSurvivorError) {
            this.setState({
              snackbarOpen: true,
              snackbarMessage: "User added successfully"
            });
          } else {
            this.setState({
              snackbarOpen: true,
              snackbarMessage: "We could not Add the user. Please try later."
            });
          }
          this.props.onGetSurvivors();
        })
    );
  };

  onCoordinatesChange = coord => {
    let coordinates = {
      lat: coord.lat,
      lng: coord.lng
    };
    this.props.onLocationChange(coordinates, "choosen");
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      snackbarOpen: false,
      snackbarMessage: undefined
    });
  };

  render() {
    const mapStyle = {
      width: "100%",
      height: "430px"
    };

    return (
      <div>
        <SurvivorForm
          onSubmit={this.submitSurvivor}
          coordinates={this.state.coordinates}
          styles={mapStyle}
          onCoordinatesChange={this.onCoordinatesChange}
          deviceCoordinates={this.props.defaultLocation}
          choosenCoordinates={this.props.choosenLocation}
        />
        <SnackBar
          open={this.state.snackbarOpen}
          handleClose={this.handleSnackBarClose}
          message={this.state.snackbarMessage}
        />
      </div>
    );
  }
}

const { func, object, bool } = PropTypes;

AddSurvivors.propTypes = {
  onAddSurvivor: func,
  onReset: func,
  onGetSurvivors: func,
  onLocationChange: func,
  onLocationClean: func,
  defaultLocation: object,
  choosenLocation: object,
  addSurvivorError: bool
};

const mapStateToProps = (state, props) => ({
  defaultLocation: state.survivors.newSurvivorLocation.default,
  choosenLocation: state.survivors.newSurvivorLocation.choosen,
  addSurvivorError: state.survivors.addSurvivorError
});

const mapDispatchToProps = dispatch => ({
  onAddSurvivor: data => dispatch(addSurvivor(data)),
  onReset: form => dispatch(reset(form)),
  onGetSurvivors: () => dispatch(getSurvivors()),
  onLocationChange: (location, type) => dispatch(setLocation(location, type)),
  onLocationClean: () => dispatch(cleanLocation())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddSurvivors);
