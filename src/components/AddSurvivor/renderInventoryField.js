import React from "react";
import PropTypes from "prop-types";
import TextField from "material-ui/TextField";
import { InputAdornment } from "material-ui/Input";
import { FormControl, FormHelperText } from "material-ui/Form";

const renderInventoryField = ({
  input,
  label,
  max,
  placeholder,
  className,
  adornment,
  meta: { touched, error },
  ...custom
}) => (
  <FormControl className={className}>
    <TextField
      label={label}
      placeholder="0"
      error={!!(touched && error)}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">{adornment}</InputAdornment>
        )
      }}
      // eslint-disable-next-line
      inputProps={{
        min: 0
      }}
      {...input}
      {...custom}
    />
    {touched &&
      error && (
        <FormHelperText id="inventory-error-text">{error}</FormHelperText>
      )}
  </FormControl>
);

const { object, string, number, element } = PropTypes;

renderInventoryField.propTypes = {
  input: object,
  label: string,
  max: number,
  placeholder: string,
  className: string,
  adornment: element,
  meta: object
};

export default renderInventoryField;
