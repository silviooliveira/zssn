import React from "react";
import PropTypes from "prop-types";
import { RadioGroup } from "material-ui/Radio";
import { FormLabel, FormControl, FormHelperText } from "material-ui/Form";

const renderRadioGroup = ({
  input,
  meta: { touched, error },
  className,
  title,
  labelClass,
  ...rest
}) => (
  <FormControl
    component="fieldset"
    className={className}
    error={!!(touched && error)}
  >
    <FormLabel component="legend" className={labelClass}>
      {title}
    </FormLabel>
    <RadioGroup
      style={{ flexDirection: "row" }}
      {...input}
      {...rest}
      value={input.value}
      onChange={(event, value) => input.onChange(value)}
    />
    {touched &&
      error && <FormHelperText id="name-error-text">{error}</FormHelperText>}
  </FormControl>
);

const { object, string } = PropTypes;

renderRadioGroup.propTypes = {
  input: object,
  meta: object,
  title: string,
  className: string,
  labelClass: string
};

export default renderRadioGroup;
