export default function(values) {
  const errors = {};
  const requiredFields = ["name", "age", "sex"];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = "Required field";
    }
  });
  return errors;
}
