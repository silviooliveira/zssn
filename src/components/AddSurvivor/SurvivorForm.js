import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";

import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Paper from "material-ui/Paper";
import Radio from "material-ui/Radio";
import { FormControlLabel } from "material-ui/Form";
import Button from "material-ui/Button";
import { Water, Pistol, Pill, Food } from "mdi-material-ui";

import validate from "./validate";
import MapContainer from "../general/GoogleMap";
import renderTextField from "./renderTextField";
import renderInventoryField from "./renderInventoryField";
import renderRadioGroup from "./renderRadioGroup";
import Loader from "../general/Loader";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    margin: `${theme.spacing.unit * 3}px auto 0 auto`,
    maxWidth: 800
  }),
  fieldset: {
    display: "flex",
    width: `calc(100% + 16px)`,
    margin: "0 -8px 24px -8px"
  },
  name: {
    flexBasis: "50%",
    padding: "0 15px"
  },
  age: {
    flexBasis: "25%",
    padding: "0 15px"
  },
  sex: {
    flexBasis: "35%",
    padding: "0 15px",
    flexDirection: "row"
  },
  sexLabel: {
    transform: "scale(0.75) translateX(-10.5px)"
  },
  inventoryField: {
    flexBasis: "25%",
    padding: "0 15px"
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: theme.typography.fontWeightMedium,
    margin: "0 0 25px 6px",
    fontSize: theme.typography.caption.fontSize,
    textTransform: "uppercase"
  },
  locationContainer: {
    display: "flex",
    justifyContent: "space-between",
    margin: "8px 0"
  },
  locationBox: {
    flexBasis: "49%",
    backgroundColor: theme.palette.grey[600],
    padding: "8px 16px",
    borderRadius: "2px",
    boxShadow: theme.shadows[1]
  },
  locationTitle: {
    textTransform: "uppercase",
    margin: "8px 0"
  },
  locationData: {
    margin: 0,
    fontStyle: "italic",
    fontSize: "0.9rem"
  },
  button: {
    margin: theme.spacing.unit
  }
});

const SurvivorForm = props => {
  const {
    handleSubmit,
    pristine,
    reset,
    submitting,
    deviceCoordinates,
    choosenCoordinates
  } = props;

  const adornments = {
    Water: <Water />,
    Food: <Food />,
    Ammunition: <Pistol />,
    Medication: <Pill />
  };

  const fields = ["Water", "Food", "Medication", "Ammunition"].map(
    (value, key) => (
      <Field
        key={value}
        name={value.toLowerCase()}
        component={renderInventoryField}
        label={value}
        min={0}
        adornment={adornments[value]}
        type="number"
        className={props.classes.inventoryField}
        InputLabelProps={{
          shrink: true
        }}
      />
    )
  );

  return (
    <Paper className={props.classes.root} elevation={4}>
      <form onSubmit={handleSubmit}>
        <Typography
          className={props.classes.title}
          variant="subheading"
          gutterBottom
        >
          General Info
        </Typography>
        <div className={props.classes.fieldset}>
          <Field
            name="name"
            component={renderTextField}
            label="Name"
            placeholder="Type your name"
            type="text"
            InputLabelProps={{
              shrink: true
            }}
            className={props.classes.name}
          />
          <Field
            name="sex"
            title="Gender"
            component={renderRadioGroup}
            className={props.classes.sex}
            labelClass={props.classes.sexLabel}
          >
            <FormControlLabel value="M" control={<Radio />} label="male" />
            <FormControlLabel value="F" control={<Radio />} label="female" />
          </Field>
          <Field
            name="age"
            component={renderTextField}
            label="Age"
            placeholder="Type your age"
            type="number"
            min="0"
            InputLabelProps={{
              shrink: true
            }}
            className={props.classes.age}
          />
        </div>
        <Typography
          className={props.classes.title}
          variant="subheading"
          gutterBottom
        >
          Inventory Items
        </Typography>
        <div className={props.classes.fieldset}>{fields}</div>
        <Typography
          className={props.classes.title}
          variant="subheading"
          gutterBottom
        >
          Location
        </Typography>
        <div
          className={props.classes.fieldset}
          style={{ paddingLeft: 14, paddingRight: 14, flexDirection: "column" }}
        >
          {deviceCoordinates ? (
            <Fragment>
              <MapContainer
                location={{
                  lat: deviceCoordinates.lat,
                  lng: deviceCoordinates.lng
                }}
                styles={props.styles}
                onCoordinatesChange={props.onCoordinatesChange}
              />
              <div className={props.classes.locationContainer}>
                <div className={props.classes.locationBox}>
                  <h6 className={props.classes.locationTitle}>Latitude</h6>
                  <p className={props.classes.locationData}>
                    {choosenCoordinates
                      ? choosenCoordinates.lat
                      : "Please drag the marker to change the location"}
                  </p>
                </div>
                <div className={props.classes.locationBox}>
                  <h6 className={props.classes.locationTitle}>Longitude</h6>
                  <p className={props.classes.locationData}>
                    {choosenCoordinates
                      ? choosenCoordinates.lng
                      : "Please drag the marker to change the location"}
                  </p>
                </div>
              </div>
            </Fragment>
          ) : (
            <Loader />
          )}
        </div>
        <div className={props.classes.fieldset}>
          <Button
            variant="raised"
            color="primary"
            disabled={pristine || submitting}
            className={props.classes.button}
            type="submit"
          >
            Submit Survivor
          </Button>
          <Button
            variant="raised"
            color="secondary"
            disabled={pristine || submitting}
            className={props.classes.button}
            onClick={reset}
          >
            Clear Values
          </Button>
        </div>
      </form>
    </Paper>
  );
};

const { func, bool, object } = PropTypes;

SurvivorForm.propTypes = {
  handleSubmit: func.isRequired,
  pristine: bool.isRequired,
  reset: func.isRequired,
  submitting: bool.isRequired,
  classes: object,
  onCoordinatesChange: func.isRequired,
  deviceCoordinates: object,
  choosenCoordinates: object,
  styles: object.isRequired
};

export default withStyles(styles)(
  reduxForm({
    form: "addSurvivorForm", // a unique identifier for this form
    validate
  })(SurvivorForm)
);
