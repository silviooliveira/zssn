import React from "react";
import PropTypes from "prop-types";
import { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import { NavLink } from "react-router-dom";
import { withStyles } from "material-ui/styles";

const style = theme => ({
  ListItem: {
    borderRadius: 3,

    "&.active": {
      backgroundColor: theme.palette.secondary.main
    }
  },
  listItemText: {
    color: theme.palette.primary.contrastText,
    fontWeight: theme.typography.fontWeightLight
  }
});

const NavItem = props => (
  <ListItem
    className={props.classes.ListItem}
    button
    component={NavLink}
    to={props.to}
  >
    <ListItemIcon color="secondary">{props.icon}</ListItemIcon>
    <ListItemText
      primary={props.title}
      disableTypography
      className={props.classes.listItemText}
    />
  </ListItem>
);

NavItem.propTypes = {
  classes: PropTypes.object.isRequired,
  to: PropTypes.string,
  icon: PropTypes.element,
  title: PropTypes.string
};

export default withStyles(style)(NavItem);
