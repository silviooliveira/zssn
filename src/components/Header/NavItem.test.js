import React from "react";
import NavItem from "./NavItem";
import { Home } from "mdi-material-ui";
import { createShallow } from "material-ui/test-utils";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

// Setup enzyme's react adapter
configure({ adapter: new Adapter() });

describe("<Index />", () => {
  let shallow;

  beforeEach(() => {
    shallow = createShallow({ dive: true });
  });

  const props = {
    to: "/home",
    icon: <Home />,
    title: "Home"
  };

  it("should render a button", () => {
    let wrapper = shallow(<NavItem {...props} />);

    expect(wrapper.first().props()).toHaveProperty("button", true);
  });
});
