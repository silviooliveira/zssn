import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";

const styles = theme => ({
  appBar: {
    width: `calc(100% - 260px)`,
    height: theme.spacing.unit * 11,
    marginLeft: 260,
    backgroundColor: theme.palette.secondary.main,
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "center"
  },
  title: {
    fontWeight: theme.typography.fontWeightLight
  }
});

const ROUTES = {
  add: "Add Survivor",
  survivors: "Survivors",
  report: "Report Infected",
  update: "Update Location",
  trade: "Trade Items",
  reports: "Reports",
  none: "Home"
};

const getTitle = (pathname, object) => {
  const name = pathname.substr(1);
  const routes = object;
  if (!name) {
    return routes.none;
  }
  return routes[name];
};

const TopBar = props => {
  const title = getTitle(props.location.pathname, ROUTES);
  return (
    <AppBar position="absolute" className={props.classes.appBar}>
      <Toolbar>
        <h2>
          <span className={props.classes.title}>
            Zombie Survival Social Network
          </span>{" "}
          |
          {title && <span>{` ${title}`}</span>}
        </h2>
      </Toolbar>
    </AppBar>
  );
};

TopBar.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(TopBar));
