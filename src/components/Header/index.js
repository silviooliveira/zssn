import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Drawer from "material-ui/Drawer";
import Divider from "material-ui/Divider";
import List from "material-ui/List";
import Typography from "material-ui/Typography";
import {
  Home,
  Account,
  AccountPlus,
  Bug,
  MapMarker,
  Autorenew,
  FileChart
} from "mdi-material-ui";

import Background from "../../assets/background.jpg";
import Logo from "../../assets/zssn.svg";
import NavItem from "./NavItem";

const drawerWidth = 260;

const styles = theme => ({
  drawerPaper: {
    position: "fixed",
    width: drawerWidth,
    backgroundImage: `url(${Background}), linear-gradient(#212121, rgba(255, 255, 255, 0.08))`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundBlendMode: "overlay"
  },
  toolbar: {
    height: theme.spacing.unit * 11,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  list: {
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit
  },
  logo: {
    display: "block",
    width: "70%",
    margin: "0 auto"
  },
  signature: {
    position: "absolute",
    bottom: 20,
    left: "50%",
    transform: "translateX(-50%)",
    width: 250,
    "& a": {
      textTransform: "uppercase",
      color: theme.palette.text.secondary
    }
  }
});

const Header = props => (
  <Drawer
    variant="permanent"
    classes={{
      paper: props.classes.drawerPaper
    }}
  >
    <div className={props.classes.toolbar}>
      <img className={props.classes.logo} src={Logo} alt="zssn logo" />
    </div>
    <Divider />
    <List className={props.classes.list}>
      <NavItem to="/home" icon={<Home />} title="Home" />
      <NavItem to="/survivors" icon={<Account />} title="Survivors" />
      <NavItem to="/add" icon={<AccountPlus />} title="Add Survivor" />
      <NavItem to="/report" icon={<Bug />} title="Report Infected" />
      <NavItem to="/update" icon={<MapMarker />} title="Update Location" />
      <NavItem to="/trade" icon={<Autorenew />} title="Trade Items" />
      <NavItem to="/reports" icon={<FileChart />} title="Reports" />
    </List>
    <Typography
      variant="body1"
      gutterBottom
      align="center"
      color="textSecondary"
      className={props.classes.signature}
    >
      © 2018 <a href="http://silviojoliveira.com">Silvio Oliveira</a>
    </Typography>
  </Drawer>
);

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
