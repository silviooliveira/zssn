import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ConnectedApp from "./App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<ConnectedApp />, document.getElementById("root"));
registerServiceWorker();
