import blue from "material-ui/colors/blue";
import orange from "material-ui/colors/orange";

// A theme with custom primary and secondary color.
// It's optional.
const palette = {
  type: "dark",
  primary: blue,
  secondary: orange
};

export { palette };
