import { GET_REPORTS, GET_REPORTS_ERROR } from "./actionTypes";
import { DEFAULT_URL } from "./defaultUrl.js";
import axios from "axios";

export const getReports = () => async dispatch => {
  const REPORTS = [
    `${DEFAULT_URL}report/infected`,
    `${DEFAULT_URL}report/non_infected`,
    `${DEFAULT_URL}report/people_inventory`,
    `${DEFAULT_URL}report/infected_points`
  ];

  const headers = {
    Accept: "application/json"
  };

  const titles = [];

  const promises = REPORTS.map(item => {
    titles.push(item.substr(item.indexOf("report/") + 7));
    return axios.get(item, headers);
  });

  try {
    const response = await Promise.all(promises);

    const reportObject = {};
    for (let i = 0; i < response.length; i++) {
      reportObject[titles[i]] = response[i].data.report;
    }

    dispatch({ type: GET_REPORTS, payload: reportObject });
  } catch (e) {
    dispatch({ type: GET_REPORTS_ERROR, payload: e });
  }
};
