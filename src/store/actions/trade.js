import {
  GET_PROPERTIES,
  GET_PROPERTIES_ERROR,
  CLEAN_PROPERTIES,
  TRADE_PROPERTIES,
  SAVE_TOTALS,
  TRADE_PROPERTIES_ERROR,
  GET_LABELS,
  SAVE_ACTIVE_PROPERTIES,
  CLEAR_ACTIVE_PROPERTIES
} from "./actionTypes";
import { DEFAULT_URL, HEADERS } from "./defaultUrl.js";
import axios from "axios";
import _ from "lodash";

export const saveTotals = (type, value) => dispatch => {
  dispatch({ type: SAVE_TOTALS, payload: { type, value } });
};

export const getProperties = (id, name) => async dispatch => {
  const headers = {
    Accept: "application/json"
  };

  const url = `${DEFAULT_URL}people/${id}/properties.json`;

  try {
    const response = await axios.get(url, headers);

    const properties = {};

    for (const object of response.data) {
      const name = object.item.name;
      properties[name] = {};
      properties[name].quantity = object.quantity;
      properties[name].pointPerItem = object.item.points;
      properties[name].totalPoints = object.quantity * object.item.points;
    }

    dispatch({ type: GET_PROPERTIES, payload: properties, name });
  } catch (e) {
    dispatch({ type: GET_PROPERTIES_ERROR, payload: e });
  }
};

export const tradeProperties = (sellerId, data) => async dispatch => {
  const url = `${DEFAULT_URL}people/${sellerId}/properties/trade_item.json`;

  try {
    await axios.post(url, data, HEADERS);
    dispatch({ type: TRADE_PROPERTIES });
  } catch (e) {
    dispatch({ type: TRADE_PROPERTIES_ERROR });
  }
};

export const cleanProperties = () => dispatch => {
  dispatch({ type: CLEAN_PROPERTIES });
};

export const getLabels = () => (dispatch, getState) => {
  const { survivors } = getState();

  const suggestions = survivors.survivors.map(item => ({
    label: item.name,
    id: item.id
  }));

  const users = _.keyBy(suggestions, "label");

  dispatch({ type: GET_LABELS, payload: users });
};

export const saveActiveProperties = (value, type) => (dispatch, getState) => {
  const { trade } = getState();

  const obj = trade.properties[value];

  dispatch({ type: SAVE_ACTIVE_PROPERTIES, payload: obj, category: type });
};

export const clearActiveProperties = type => dispatch => {
  dispatch({ type: CLEAR_ACTIVE_PROPERTIES, payload: type });
};
