import {
  GET_SURVIVORS,
  GET_SURVIVORS_REQUESTED,
  ADD_SURVIVOR,
  ADD_SURVIVOR_REQUESTED,
  ADD_SURVIVOR_ERROR,
  UPDATE_SURVIVOR,
  UPDATE_SURVIVOR_ERROR,
  SET_LOCATION,
  CLEAN_LOCATION,
  SET_ACTIVE_LOCATION,
  CLEAN_ACTIVE_LOCATION,
  CHANGE_ACTIVE_LOCATION,
  SURVIVOR_ERROR
} from "./actionTypes";
import { DEFAULT_URL, HEADERS } from "./defaultUrl.js";
import axios from "axios";

import { getCoordinates } from "../../helpers";

export const getSurvivors = () => async dispatch => {
  dispatch({ type: GET_SURVIVORS_REQUESTED });

  const url = `${DEFAULT_URL}people.json`;

  try {
    const response = await fetch(url);
    const json = await response.json();

    const suggestions = json.map(item => {
      const string = item.location;
      const id = string.substring(string.length - 36);
      return {
        ...item,
        id
      };
    });

    dispatch({ type: GET_SURVIVORS, payload: suggestions });
  } catch (e) {
    dispatch({ type: SURVIVOR_ERROR, payload: e });
  }
};

export const addSurvivor = data => async dispatch => {
  dispatch({ type: ADD_SURVIVOR_REQUESTED });

  const url = `${DEFAULT_URL}people.json`;

  try {
    const response = await axios.post(url, data, HEADERS);
    dispatch({ type: ADD_SURVIVOR, payload: response });
  } catch (e) {
    dispatch({ type: ADD_SURVIVOR_ERROR, payload: e });
  }
};

export const updateSurvivor = (id, data) => async dispatch => {
  const url = `${DEFAULT_URL}people/${id}`;

  try {
    await axios.patch(url, data, HEADERS);
    dispatch({ type: UPDATE_SURVIVOR });
  } catch (e) {
    dispatch({ type: UPDATE_SURVIVOR_ERROR, payload: e });
  }
};

export const setLocation = (location, category) => dispatch => {
  dispatch({ type: SET_LOCATION, payload: location, category });
};

export const cleanLocation = () => dispatch => {
  dispatch({ type: CLEAN_LOCATION });
};

export const setActiveLocation = name => (dispatch, getState) => {
  const { survivors } = getState();

  const survivorObject = survivors.survivors.filter(
    item => item.name === name
  )[0];

  const survivorLocation = survivorObject.location;

  const survivorID = survivorLocation.substring(survivorLocation.length - 36);

  const coordinates = getCoordinates(survivorObject.lonlat);

  const activeSurvivor = {
    ...survivorObject,
    id: survivorID,
    coordinates
  };

  dispatch({ type: SET_ACTIVE_LOCATION, payload: activeSurvivor });
};

export const changeActiveLocation = coord => (dispatch, getState) => {
  const { survivors } = getState();

  const activeSurvivor = {
    ...survivors.activeSurvivor,
    coordinates: {
      lat: coord.lat,
      lng: coord.lng
    }
  };

  dispatch({ type: CHANGE_ACTIVE_LOCATION, payload: activeSurvivor });
};

export const cleanActiveLocation = () => dispatch => {
  dispatch({ type: CLEAN_ACTIVE_LOCATION });
};
