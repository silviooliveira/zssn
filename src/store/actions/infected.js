import { FLAG_SURVIVOR, FLAG_SURVIVOR_ERROR } from "./actionTypes";
import { DEFAULT_URL, HEADERS } from "./defaultUrl.js";
import axios from "axios";

export const flagSurvivor = (survivor, infected) => async dispatch => {
  const url = `${DEFAULT_URL}people/${survivor}/report_infection.json`;

  try {
    await axios.post(url, infected, HEADERS);
    dispatch({ type: FLAG_SURVIVOR });
  } catch (e) {
    dispatch({ type: FLAG_SURVIVOR_ERROR });
  }
};
