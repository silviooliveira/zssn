import { createStore, combineReducers, applyMiddleware } from "redux";
import { reducer as formReducer } from "redux-form";
import thunk from "redux-thunk";

import survivorsReducer from "./reducers/survivors";
import tradeReducer from "./reducers/trade";
import infectedReducer from "./reducers/infected";
import reportsReducer from "./reducers/reports";

const rootReducer = combineReducers({
  survivors: survivorsReducer,
  trade: tradeReducer,
  infected: infectedReducer,
  reports: reportsReducer,
  form: formReducer
});

const configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;
