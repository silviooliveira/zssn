import { GET_REPORTS, GET_REPORTS_ERROR } from "../actions/actionTypes";

const initialState = {
  reports: {},
  reportsError: false
};

const reportsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REPORTS:
      return {
        ...state,
        reportsError: false,
        reports: action.payload
      };

    case GET_REPORTS_ERROR:
      return {
        ...state,
        reportsError: true
      };

    default:
      return state;
  }
};

export default reportsReducer;
