import { FLAG_SURVIVOR, FLAG_SURVIVOR_ERROR } from "../actions/actionTypes";

const initialState = {
  flagSurvivorError: false
};

const infectedReducer = (state = initialState, action) => {
  switch (action.type) {
    case FLAG_SURVIVOR:
      return {
        ...state,
        flagSurvivorError: false
      };

    case FLAG_SURVIVOR_ERROR:
      return {
        ...state,
        flagSurvivorError: true
      };

    default:
      return state;
  }
};

export default infectedReducer;
