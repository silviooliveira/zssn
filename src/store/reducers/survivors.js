import {
  GET_SURVIVORS,
  GET_SURVIVORS_REQUESTED,
  ADD_SURVIVOR_REQUESTED,
  ADD_SURVIVOR_ERROR,
  SET_LOCATION,
  CLEAN_LOCATION,
  SET_ACTIVE_LOCATION,
  CLEAN_ACTIVE_LOCATION,
  CHANGE_ACTIVE_LOCATION,
  SURVIVOR_ERROR,
  UPDATE_SURVIVOR,
  UPDATE_SURVIVOR_ERROR
} from "../actions/actionTypes";

const initialState = {
  survivors: [],
  survivorsLoading: false,
  survivorsLoaded: false,
  newSurvivorLocation: {
    default: undefined,
    choosen: undefined
  },
  activeSurvivor: null,
  survivorsError: false,
  addSurvivorError: false,
  updateSurvivorError: false
};

const survivorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SURVIVORS_REQUESTED:
      return {
        ...state,
        survivorsLoading: true
      };

    case GET_SURVIVORS:
      return {
        ...state,
        survivors: action.payload,
        survivorsLoading: !state.survivorsLoading,
        survivorsLoaded: true,
        survivorsError: false
      };

    case SURVIVOR_ERROR:
      return {
        ...state,
        survivorsError: true
      };

    case ADD_SURVIVOR_REQUESTED:
      return {
        ...state,
        survivorAdding: true,
        addSurvivorError: false
      };
    case ADD_SURVIVOR_ERROR:
      return {
        ...state,
        addSurvivorError: true
      };
    case UPDATE_SURVIVOR:
      return {
        ...state,
        updateSurvivorError: false
      };

    case UPDATE_SURVIVOR_ERROR:
      return {
        ...state,
        updateSurvivorError: true
      };

    case SET_LOCATION:
      return {
        ...state,
        newSurvivorLocation: {
          ...state.newSurvivorLocation,
          [action.category]: action.payload
        }
      };

    case CLEAN_LOCATION:
      return {
        ...state,
        newSurvivorLocation: {
          default: undefined,
          choosen: undefined
        }
      };

    case SET_ACTIVE_LOCATION:
      return {
        ...state,
        activeSurvivor: action.payload
      };

    case CLEAN_ACTIVE_LOCATION:
      return {
        ...state,
        activeSurvivor: null
      };

    case CHANGE_ACTIVE_LOCATION:
      return {
        ...state,
        activeSurvivor: action.payload
      };

    default:
      return state;
  }
};

export default survivorsReducer;
