import {
  GET_PROPERTIES,
  GET_PROPERTIES_ERROR,
  CLEAN_PROPERTIES,
  TRADE_PROPERTIES,
  TRADE_PROPERTIES_ERROR,
  SAVE_TOTALS,
  GET_LABELS,
  SAVE_ACTIVE_PROPERTIES,
  CLEAR_ACTIVE_PROPERTIES
} from "../actions/actionTypes";

const initialState = {
  properties: {},
  getPropertiesError: false,
  totals: {
    buyer: 0,
    seller: 0
  },
  tradeError: false,
  labels: null,
  activeProperties: {
    buyer: null,
    seller: null
  }
};

const tradeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES:
      return {
        ...state,
        getPropertiesError: false,
        properties: {
          ...state.properties,
          [action.name]: action.payload
        }
      };

    case GET_PROPERTIES_ERROR:
      return {
        ...state,
        getPropertiesError: true
      };

    case CLEAN_PROPERTIES:
      return {
        ...state,
        properties: {}
      };

    case SAVE_TOTALS:
      const totals = {
        ...state.totals,
        [action.payload.type]: action.payload.value
      };
      return {
        ...state,
        totals
      };

    case TRADE_PROPERTIES:
      return {
        ...state,
        tradeError: false
      };

    case TRADE_PROPERTIES_ERROR:
      return {
        ...state,
        tradeError: true
      };

    case GET_LABELS:
      return {
        ...state,
        labels: action.payload
      };

    case SAVE_ACTIVE_PROPERTIES:
      return {
        ...state,
        activeProperties: {
          ...state.activeProperties,
          [action.category]: action.payload
        }
      };

    case CLEAR_ACTIVE_PROPERTIES:
      return {
        ...state,
        activeProperties: {
          ...state.activeProperties,
          [action.payload]: null
        }
      };

    default:
      return state;
  }
};

export default tradeReducer;
