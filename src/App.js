import React from "react";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";

import CssBaseline from "material-ui/CssBaseline";
import { MuiThemeProvider, createMuiTheme } from "material-ui/styles";

import Routes from "./Routes";

import * as myTheme from "./theme";

const store = configureStore();

const theme = createMuiTheme(myTheme);

const App = props => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Routes />
  </MuiThemeProvider>
);

const ConnectedApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

export default ConnectedApp;
